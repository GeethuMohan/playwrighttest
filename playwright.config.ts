import * as dotEnv from 'dotenv';

// playwright.config.ts
import { PlaywrightTestConfig } from '@playwright/test';

dotEnv.config();

const config: PlaywrightTestConfig = {
    // globalSetup: require.resolve('./global-setup'),
    // reporter: [
    //     ['line'],
    //     ['experimental-allure-playwright']
    // ],
    // reporter: [['line', './src/services/reporter.service.ts']],
    // reporter: './src/services/reporter.service.ts',
    workers: 2,
    projects: [
        {
            name: 'test',
            retries: 1,
            use: {
                // Configure the browser to use.
                browserName: 'chromium',
                headless: true,
                baseURL: process.env.URL,
            },
            testDir: './',
        },
    ]
};
export default config;