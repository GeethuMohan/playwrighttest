let CronJob = require('cron').CronJob;
let slack = require('@gemone/automation').SlackMessageGenerator;

const Server = {
    init: () => {

    },
    start: () => {
        createCronJob('3 5-22 * * 1-5', ['run', 'test']);
    },
};

try {
    Server.init();
    Server.start();
} catch (error) {
    console.error(`error when initializing/starting application: \n${error}`);
}

function createCronJob(crone, script) {
    return new CronJob(crone, async function () {
        const { spawn } = require('child_process');
        var spawns = spawn('npm', script)
        spawns.stdout.on('data', function (msg) {
            console.log(msg.toString());
            if (msg.toString().includes('Error code')) {
                slack.chromeNotInstalledMessage('default');
            }
        })

        spawns.stderr.on('error', (error) => {
            console.log(error.toString())
        })

    }, null, true, 'Europe/Brussels');
}